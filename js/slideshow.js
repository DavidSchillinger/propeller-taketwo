/*global angular*/

var app = angular.module('angular', [])

app.controller('slidesController', function($scope) {
    $scope.image = '/media/slideshow1.jpg'
    
    function nextImage(currentImage) {
        var image = currentImage.charAt(currentImage.length - 5)
        if (image === '6') {
            return '/media/slideshow1.jpg'
        } else {
            var imageId = parseInt(image) +1
            return '/media/slideshow'+imageId+'.jpg'
        }
    }
    
    $scope.slideshow = function($event) {
        $scope.image = nextImage($scope.image)
    }
    $scope.image1 = function($event) {
        $scope.image = '/media/slideshow1.jpg'
    }
    $scope.image2 = function($event) {
        $scope.image = '/media/slideshow2.jpg'
    }
    $scope.image3 = function($event) {
        $scope.image = '/media/slideshow3.jpg'
    }
    $scope.image4 = function($event) {
        $scope.image = '/media/slideshow4.jpg'
    }
    $scope.image5 = function($event) {
        $scope.image = '/media/slideshow5.jpg'
    }
    $scope.image6 = function($event) {
        $scope.image = '/media/slideshow6.jpg'
    }
})