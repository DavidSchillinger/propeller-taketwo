This is take two on the PBT-Propeller landing page.
The first try was a mess so this one will be responsive.

This time with more % widths in order to scale down with window size.
Media @ queries in order to change behaviour when the window size drop too low. (mobile devices.)
I will be making use of the Firefox built in developer tools (Responsive Design Mode) to test different screen sizes.
Common screen sizes are: 1920x1200, 1920x1080, 1280x720, 1024x768, 1334x750, 2048×1536, 1366x768, 1280x1024, 1280x800, 1680x1050, 1440x900, 1600x900.
Note: Test all in portrait mode too.
According to W3Schools, "As of today, about 97% of our visitors have a screen resolution of 1024x768 pixels or higher."